<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WEBMAG - @yield('titulo')</title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    
</head>
<body>
    <div class="container">
        <div class="col-12">
    <div id="geral">
            <header id="cabecalho">
                    <h1><img src="img/logo.png" alt="" width="140"></h1>
                        <nav>
                            <ul>
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('html')}}">HTML</a></li>
                                <li><a href="{{route('javascript')}}">JAVASCRIPT</a></li>
                                <li><a href="{{route('dica-css')}}">CSS</a></li>
                                <li><a href="{{route('video-aula')}}">VÍDEOS AULAS</a></li>
                                <li><a href="{{route('contato')}}">CONTATO</a></li>
                            </ul>
                        </nav>
                  </header>
                </div>
            </div>

                

                    @yield('conteudo')
                                                    
                <div class="container">
                    <div class="rodape">
                    <div class="row">
                        <div class="col-12">
                        
                                <p class="pp">TODOS OS DIREITOS SÃO RESERVADOS</p>   
                        </div>
                    </div>
                    </div>
                </div>
                
                
    
</body>
</html>