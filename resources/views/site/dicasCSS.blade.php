@extends('layouts.home')

@section('titulo','CSS')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-5">
            <img src="img/post-5.jpg" alt="" width="300">
        </div>

        <div class="col-7">
            <h2>Planejamento</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere quidem beatae tempore, culpa, magni reiciendis error, pariatur doloremque ab iure hic enim laboriosam. Ea, voluptatem quisquam dolore magni sapiente nisi.</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit sequi tempora libero cupiditate eaque atque dignissimos id, dicta sint est culpa, ut magnam ea doloribus incidunt voluptates optio, aut dolorum.</p>
        </div>
    </div>
</div>

@endsection