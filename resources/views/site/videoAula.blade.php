@extends('layouts.home')

@section('titulo','')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-4">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="col-8">
              <h1>Curso de HTML e CSS para iniciantes - Aula 1</h1>

              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Pariatur labore quidem dolores doloribus veniam incidunt fuga quisquam saepe commodi, doloremque veritatis, architecto perspiciatis possimus eos quibusdam voluptates quos qui odio.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-4">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>GitHub, GitLab ou Bitbucket? Qual nós usamos! // CAC #6</p>
        </div>

        <div class="col-4">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <P>MVC // Dicionário do Programador</P>
        </div>

        <div class="col-4">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>O que um Analista de Sistemas faz? // Vlog #77</p>
        </div>
    </div>
</div>

@endsection