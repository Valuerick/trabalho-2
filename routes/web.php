<?php



Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('html', function () {
    return view('site.html');
})->name('html');

Route::get('javascript', function () {
    return view('site.codigoJavascript');
})->name('javascript');

Route::get('dica-css', function () {
    return view('site.dicasCSS');
})->name('dica-css');

Route::get('video-aulas', function () {
    return view('site.videoAula');
})->name('video-aula');

Route::get('contato', function () {
    return view('site.contato');
})->name('contato');
